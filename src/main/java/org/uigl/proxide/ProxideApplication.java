package org.uigl.proxide;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import org.apache.commons.codec.binary.Base64;
import org.littleshoot.proxy.*;
import org.littleshoot.proxy.impl.DefaultHttpProxyServer;

import java.io.*;
import java.net.*;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProxideApplication {

    static String PROXIDE_AUTH_STRING = "Basic " + Base64.encodeBase64String((System.getenv("PROXIDE_USER") + ":" + System.getenv("PROXIDE_PASS")).getBytes());
    static int PROXIDE_PORT = Integer.valueOf(System.getenv("PROXIDE_PORT"));
    static String PROXIDE_HOST = System.getenv("PROXIDE_HOST");
    static String PROXIDE_NONHOSTS[] = createNonProxyHostMatchers(System.getenv("PROXIDE_NONHOSTS"));

    private static String[] createNonProxyHostMatchers(String env) {
        if (env == null || env.isEmpty() || env.trim().isEmpty()) {
            return new String[]{};
        }

        String nonProxyHosts[] = env.split("\\s*,\\s*");
        String nonProxyHostMatcher[] = new String[nonProxyHosts.length];
        for (int i=0; i<nonProxyHosts.length; i++) {
            nonProxyHostMatcher[i] = wildcardMatcher(nonProxyHosts[i]);
        }

        return nonProxyHostMatcher;
    }

    private static String wildcardMatcher(String value) {
        Pattern regex = Pattern.compile("[^*]+|(\\*)");
        Matcher m = regex.matcher(value);
        StringBuffer b = new StringBuffer();
        while (m.find()) {
            if (m.group(1) != null) {
                m.appendReplacement(b, ".*");
            } else {
                m.appendReplacement(b, "\\\\Q" + m.group(0) + "\\\\E");
            }
        }
        m.appendTail(b);
        return b.toString();
    }

    private static ChainedProxyManager createChainProxyManager() {

        return new ChainedProxyManager() {
            public void lookupChainedProxies(HttpRequest httpRequest, Queue<ChainedProxy> chainedProxies) {

                if (PROXIDE_NONHOSTS.length > 0) {
                    for (String host : PROXIDE_NONHOSTS) {
                        if (httpRequest.getUri().matches(host)) {
                            chainedProxies.add(ChainedProxyAdapter.FALLBACK_TO_DIRECT_CONNECTION);
                        }
                    }
                }

                chainedProxies.add(new ChainedProxyAdapter() {
                    @Override
                    public InetSocketAddress getChainedProxyAddress() {
                        return new InetSocketAddress(PROXIDE_HOST, PROXIDE_PORT);
                    }
                });
            }
        };
    }

    public static void main(String[] args) throws IOException {

        HttpProxyServer server = DefaultHttpProxyServer.bootstrap()
                .withPort(PROXIDE_PORT)
                .withChainProxyManager(createChainProxyManager())
                .withTransparent(true)
                .withFiltersSource(new HttpFiltersSource() {
                    @Override
                    public HttpFilters filterRequest(HttpRequest originalRequest, ChannelHandlerContext ctx) {
                        return new HttpFiltersAdapter(originalRequest, ctx) {
                            @Override
                            public HttpResponse clientToProxyRequest(HttpObject httpObject) {
                                if (httpObject instanceof HttpRequest) {
                                    HttpRequest request = (HttpRequest) httpObject;
                                    HttpHeaders headers = request.headers();
                                    headers.remove(HttpHeaders.Names.IF_MODIFIED_SINCE);
                                    headers.remove(HttpHeaders.Names.IF_NONE_MATCH);
                                    headers.add("Proxy-Authorization", PROXIDE_AUTH_STRING);
                                }
                                //System.out.println("hit");
                                //System.out.println(httpObject.toString());
                                return null;
                            }
                        };
                    }

                    @Override
                    public int getMaximumRequestBufferSizeInBytes() {
                        return 0;
                    }

                    @Override
                    public int getMaximumResponseBufferSizeInBytes() {
                        return 0;
                    }
                })
                .start();
    }
}
